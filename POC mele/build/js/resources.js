game.resources = [
	{
		"name": "level",
		"type": "tmx",
		"src": "data/map/level.json"
	},
	{
		"name": "tileset",
		"type": "tsx",
		"src": "data/map/tileset.json"
	},
	{
		"name": "jalan",
		"type": "image",
		"src": "data/image/jalan.png"
	},
	{
		"name": "player75x96",
		"type": "image",
		"src": "data/image/player75x96.png"
	},
	{
		"name": "game_play",
		"type": "audio",
		"src": "data/audio/"
	},
	{
		"name": "tower-god",
		"type": "image",
		"src": "data/image/tower-god.png"
	},
	{
		"name": "player-spawner",
		"type": "image",
		"src": "data/image/player-spawner.png"
	},
	{
		"name": "start-button",
		"type": "image",
		"src": "data/image/start-button.png"
	},
	{
		"name": "menu",
		"type": "tmx",
		"src": "data/map/menu.json"
	},
	{
		"name": "atlas",
		"type": "json",
		"src": "data/image/atlas.json"
	},
	{
		"name": "atlas0",
		"type": "image",
		"src": "data/image/atlas0.png"
	},
	{
		"name": "atlas1",
		"type": "image",
		"src": "data/image/atlas1.png"
	}
];
