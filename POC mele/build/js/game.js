var game = {
	data : {},

	onload : function () {
		// Initialize the video.
		if (!me.video.init(4000, 2000, {wrapper : "screen", scale : "auto", scaleMethod: "fit"})) {
			alert("Your browser does not support HTML5 canvas.");
			return;
		}
		me.audio.init("mp3,ogg");

		// set and load all resources.
		// (this will also automatically switch to the loading screen)
		me.loader.preload(game.resources, this.loaded.bind(this));
	},

	// Run on game resources loaded.
	loaded : function () {
		me.state.SCREEN3 = 10;
		me.state.set(me.state.SCREEN3, new game.levelScreen());
		me.state.SCREEN18 = 11;
		me.state.set(me.state.SCREEN18, new game.menuScreen());

		me.pool.register('player', game.player);
		me.pool.register('tower', game.tower);
		me.pool.register('playerSpawner', game.playerSpawner);
		me.pool.register('playButton', game.playButton);
		me.pool.register('tower2', game.tower2);

		me.state.change(me.state.SCREEN3);
	}
};
