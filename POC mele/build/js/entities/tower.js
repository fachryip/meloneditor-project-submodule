// Put user code here //
 
//  End of user code  //
game.tower = me.Sprite.extend({
	init: function(x, y, settings = {}){
		settings.image = 'tower-god';
		settings.framewidth = 256;
		settings.frameheight = 342;
		settings.anchorPoint = {
			x: 0.5,
			y: 1
		};

		this._super(me.Sprite, 'init', [x, y, settings]);

		this.alpha = 1;
		this.floating = false;
		this.alwaysUpdate = false;
		this.updateWhenPaused = false;
		this.isPersistent = false;


		this.addAnimation('Animation 1', [0], 100);
		this.setCurrentAnimation('Animation 1');

		this.body = new me.Body(this);
		this.body.addShape( new me.Ellipse(110.5, 159.5, 1117, 1063) );
		this.isKinematic = false;
		this.body.gravity.y = 0;

		me.input.bindKey(me.input.KEY.A, "A");
		me.input.bindKey(me.input.KEY.D, "D");
		me.input.bindKey(me.input.KEY.W, "W");
		me.input.bindKey(me.input.KEY.S, "S");
		me.input.bindKey(me.input.KEY.Z, "Z");

		// Put user code here //
		this.isAlive = true;
		this.hp = this.maxHp = 10000;
		//  End of user code  //
	},

	update: function(dt){
		var drawNextFrame = this._super(me.Sprite, 'update', [dt]);

		this.body.update();
		me.collision.check(this);
		drawNextFrame = drawNextFrame || this.body.vel.x !== 0 || this.body.vel.y !== 0;
		if (me.input.isKeyPressed("A")) {
			this.body.vel.x = (-5);
		} else if (me.input.isKeyPressed("D")) {
			this.body.vel.x = 5;
		} else {
			this.body.vel.x = 0;
		}
		if (me.input.isKeyPressed("W")) {
			this.body.vel.y = (-5);
		} else if (me.input.isKeyPressed("S")) {
			this.body.vel.y = 5;
		} else {
			this.body.vel.y = 0;
		}
		if (me.input.isKeyPressed("Z")) {
		}
	
		// Put user code here //
		if (me.input.isKeyPressed("Z")) {
			this.destroyed();
		}
		//  End of user code  //
		return drawNextFrame;
	},

	onCollision : function(response, other) {
		// Put user code here //
		
		//  End of user code  //
		return false;
	},

	draw : function(renderer) {
		this._super(me.Sprite, 'draw', [renderer]);
		// Put user code here //
		var {width, height} = this.getBounds();
		var barWidth = 400;
		renderer.setColor('black');
		renderer.fillRect(this.pos.x + width/2 - barWidth/2 - 2, this.pos.y, barWidth + 4, 10);
		renderer.setColor('red');
		renderer.fillRect(this.pos.x + width/2 - barWidth/2, this.pos.y, barWidth*this.hp/this.maxHp, 10);
		//  End of user code  //
	},

	// Put user code here //
	destroyed : function() {
		this.isAlive = true;
		this.body.setCollisionMask(me.collision.types.NO_OBJECT);
	},
	
	hit : function() {
		this.hp--;
		if(this.hp <= 0) this.destroyed();
	},
	
	getSide : function(){
	    return +this.ancestor.name[5];
	}
	//  End of user code  //
});
