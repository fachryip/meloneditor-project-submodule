// Put user code here //

//  End of user code  //
game.playButton = me.GUI_Object.extend({
	init: function(x, y, settings = {}){
		settings.image = 'start-button';
		settings.framewidth = 1072;
		settings.frameheight = 497;
		settings.anchorPoint = {
			x: 0,
			y: 0
		};

		this._super(me.GUI_Object, 'init', [x, y, settings]);
		this.alpha = undefined;
		this.floating = undefined;
		this.alwaysUpdate = undefined;
		this.updateWhenPaused = undefined;
		this.isPersistent = undefined;


		this.addAnimation('Animation 1', [0], 100);
		this.setCurrentAnimation('Animation 1');

		// Put user code here //
		
		//  End of user code  //
	},

	onClick: function(pointer){
		me.state.change(me.state.SCREEN3);
	
		// Put user code here //
		
		//  End of user code  //
	},

	// Put user code here //
	
	//  End of user code  //
})
