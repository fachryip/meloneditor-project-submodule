// Put user code here //

//  End of user code  //
game.tower2 = me.Sprite.extend({
	init: function(x, y, settings = {}){
		settings.framewidth = 100;
		settings.frameheight = 100;
		settings.anchorPoint = {
			x: 0,
			y: 0
		};

		this._super(me.Sprite, 'init', [x, y, settings]);

		this.alpha = 1;
		this.floating = false;
		this.alwaysUpdate = false;
		this.updateWhenPaused = false;
		this.isPersistent = false;


		// Put user code here //
		
		//  End of user code  //
	},

	update: function(dt){
		var drawNextFrame = this._super(me.Sprite, 'update', [dt]);

		// Put user code here //
		
		//  End of user code  //
		return drawNextFrame;
	},

	draw : function(renderer) {
		this._super(me.Sprite, 'draw', [renderer]);
		// Put user code here //
		
		//  End of user code  //
	},

	// Put user code here //
	
	//  End of user code  //
});
