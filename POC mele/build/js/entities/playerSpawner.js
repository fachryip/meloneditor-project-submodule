// Put user code here //

//  End of user code  //
game.playerSpawner = me.GUI_Object.extend({
	init: function(x, y, settings = {}){
		settings.image = 'player-spawner';
		settings.framewidth = 248;
		settings.frameheight = 192;
		settings.anchorPoint = {
			x: 0.5,
			y: 0.8
		};

		this._super(me.GUI_Object, 'init', [x, y, settings]);
		this.alpha = 1;
		this.floating = false;
		this.alwaysUpdate = false;
		this.updateWhenPaused = false;
		this.isPersistent = false;


		this.addAnimation('Animation 1', [0], 100);
		this.setCurrentAnimation('Animation 1');

		// Put user code here //
		this.alpha = 0;
		this.initPath();
		//  End of user code  //
	},

	// Put user code here //
	getTileProperties: function(tile) {
		return tile.tileset.getTileProperties(tile.tileId);
	},
	
	getDirection: function(dir){
	    switch (dir) {
	        case 0: return {col: 0, row: -1};
	        case 1: return {col: 1, row: 0};
	        case 2: return {col: 0, row: 1};
	        case 3: return {col: -1, row: 0};
	        default: return false;
	    }
	},
	
	initPath: function(){
	    var level = me.levelDirector.getCurrentLevel();
	    var renderer = this.renderer = level.getRenderer();
		var roads = level.getLayers().find( layer => layer.name === 'road' );
		var roadData = roads.layerData;
		
		
		var startTile = roads.getTile(this.pos.x, this.pos.y);
		if(!startTile) return console.error('Player spawner not above startTile');
		
		var path = this.path = [{col: startTile.col, row: startTile.row}];
		var properties = this.getTileProperties(startTile);
		
		if(properties.isStart !== 'true') console.error('Player spawner not above startTile');

        var dir = this.getDirection(+properties.node1);
        
        var startNode = (+properties.node1 + 2)%4;
        var tile = roadData[startTile.col + dir.col][startTile.row + dir.row];
        properties = this.getTileProperties(tile);
        do{
            path.push({col: tile.col, row: tile.row});
            var nextNode = startNode === +properties.node1 ? +properties.node2 : +properties.node1;
            startNode = (nextNode + 2)%4;
            
            dir = this.getDirection(nextNode);
            tile = roadData[tile.col + dir.col][tile.row + dir.row];
            properties = this.getTileProperties(tile);
        }while(!properties.isStart);
        path.push({col: tile.col, row: tile.row});
        
        this.pos = renderer.tileToPixelCoords(startTile.col, startTile.row);
        this.pos.y += renderer.hTileheight;
        
        var counter = 0;
        me.timer.setInterval(() => {
            counter++;
            if(counter < 5) this.spawnPlayer();
            else if(counter > 25){
                counter = 0;
            }
        }, 200);
	},

	spawnPlayer : function(delay){
	    var player = me.pool.pull("player", this.pos.x, this.pos.y);
	    player.spawner = this;
        player.setPath(this.path.slice(0), this.renderer);
        me.game.world.addChild(player);
	}
	//  End of user code  //
})
