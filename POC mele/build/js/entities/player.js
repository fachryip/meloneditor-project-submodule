// Put user code here //
 
//  End of user code  //
game.player = me.Sprite.extend({
	init: function(x, y, settings = {}){
		settings.image = 'player75x96';
		settings.framewidth = 75;
		settings.frameheight = 96;
		settings.anchorPoint = {
			x: 0.5,
			y: 0.9
		};

		this._super(me.Sprite, 'init', [x, y, settings]);

		this.alpha = 1;
		this.floating = false;
		this.alwaysUpdate = false;
		this.updateWhenPaused = false;
		this.isPersistent = false;


		this.addAnimation('walk', [0,1,2], 100);
		this.addAnimation('idle', [0], 100);
		this.setCurrentAnimation('walk');

		this.body = new me.Body(this);
		this.body.addShape( new me.Rect(0, 0, this.width, this.height) );
		this.isKinematic = false;
		this.body.gravity.y = 0;

		// Put user code here //
		this.alwaysUpdate = true;
		this.speed = 5;
		this.target = null;
		
		var maxOffset = 30;
		this.offsetX = Math.floor(-maxOffset + Math.random()*maxOffset*2);
		this.offsetY = Math.floor(-maxOffset + Math.random()*maxOffset*2);
		
		this.font = new me.Font("Arial", 50, 'rgba(255,0,0,1)');
        
        this.isAlive = true;
		this.hp = this.maxHp = 100;
		
		this.renderRange = new me.Renderable(x-100, y-100, 200, 200);
		this.renderRange.anchorPoint.set(0, 0);
		this.renderRange.body = new me.Body(this.renderRange);
		this.renderRange.body.addShape(new me.Ellipse(0, 0, 200, 200));
		this.renderRange.isKinematic = false;
		this.renderRange.body.gravity.y = 0;
		me.game.world.addChild(this.renderRange);
		
		this.renderRange.onCollision = function(response, other) {
    		// Put user code here //
    		if(other.getSide() !== this.getSide()){
    		    this.setAtackTarget(other);
    		}
    		//  End of user code  //
    		return false;
		}.bind(this);
		this.renderRange.getSide = this.getSide.bind(this);
		
		//  End of user code  //
	},

	update: function(dt){
		var drawNextFrame = this._super(me.Sprite, 'update', [dt]);

		this.body.update();
		drawNextFrame = drawNextFrame || this.body.vel.x !== 0 || this.body.vel.y !== 0;

		// Put user code here //
        this.renderRange.pos.x = this.pos.x-100;
        this.renderRange.pos.y = this.pos.y-100;
        // this.renderRange.body.update();
        
        
        me.collision.check(this.renderRange);
        //drawNextFrame = drawNextFrame || this.renderRange.body.vel.x !== 0 || this.renderRange.body.vel.y !== 0;
        
        this.pos.z = 100 + this.offsetY;
		
		if(this.isAttacking) {
		    if(this.target.isAlive){
		        this.attack();
		        return true;
		    }
		    else {
		        this.target = null;
		        this.isAttacking = false;
		        this.setCurrentAnimation('walk');
		    }
		    
		    
		}
		if(this.nextPos){
	        var vel = this.nextPos.clone().sub(this.pos);
	        var distance = vel.length();
	        vel.normalize().scale(this.speed);
	        this.body.vel = vel;
	        this.renderRange.body.vel = vel;
	        
	        this.flipX(vel.x < 0);
	        
	        if(distance < 20) {
	            if(this.target){
	                this.body.vel.set(0, 0);
	                this.renderRange.body.vel.set(0, 0);
	                this.setCurrentAnimation('idle');
	                this.isAttacking = true;
	            }
	            else if(!this.path.length) {
	                this.ancestor.removeChild(this);
	                this.isAlive = false;
	                this.ancestor.removeChild(this.renderRange);
	            }
	            else this.setNextTarget();
	        }
		}
		//  End of user code  //
		return drawNextFrame;
	},

	onCollision : function(response, other) {
		// Put user code here //
// 		if(other.getSide() !== this.getSide()){
// 		    this.setAtackTarget(other);
// 		}
		//  End of user code  //
		return false;
	},

	draw : function(renderer) {
		this._super(me.Sprite, 'draw', [renderer]);
		// Put user code here //
		if(this.isAttacking){
		    var x = -50 + Math.random()*100;
		    var y = -50 + Math.random()*100;
		    this.font.draw(renderer, 'Atack', this.pos.x + x, this.pos.y + y);
		}
		
		var {width, height} = this.getBounds();
		var barWidth = 200;
		renderer.setColor('black');
		renderer.fillRect(this.pos.x + width/2 - barWidth/2 - 2, this.pos.y, barWidth + 4, 10);
		renderer.setColor('red');
		renderer.fillRect(this.pos.x + width/2 - barWidth/2, this.pos.y, barWidth*this.hp/this.maxHp, 10);
		//  End of user code  //
	},

	// Put user code here //
	setPath : function(path, tileRenderer){
	    this.path = path;
	    this.tileRenderer = tileRenderer;
	    this.setNextTarget();
	},
	
	setNextTarget : function(){
	    var target = this.path.shift();
		this.nextPos = this.tileRenderer.tileToPixelCoords(target.col, target.row);
		this.nextPos.x += this.offsetX;
		this.nextPos.y += this.tileRenderer.hTileheight + this.offsetY;
	},
	
	setAtackTarget : function(tower){
	    if(this.target) return;
	    
	    this.target = tower;
	    this.nextPos.x = tower.pos.x + this.offsetX;
	    this.nextPos.y = tower.pos.y + this.offsetY;
	},
	
	destroyed : function() {
		this.isAlive = false;
		this.body.setCollisionMask(me.collision.types.NO_OBJECT);
		this.renderRange.body.setCollisionMask(me.collision.types.NO_OBJECT);
		this.ancestor.removeChild(this.renderRange);
		this.ancestor.removeChild(this);
	},
	
	hit : function() {
		this.hp--;
		if(this.hp <= 0) this.destroyed();
	},
	
	attack : function(){
	    this.target.hit();
	},
	
	getSide : function(){
	    return +this.spawner.ancestor.name[6];
	}
	//  End of user code  //
});
