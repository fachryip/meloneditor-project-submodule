var idapp = '233643317198528_233948873834639';

function showAdsVideo(callback) {
        var ads;
        FBInstant.getRewardedVideoAsync(idapp).then(function(rewardedVideo) {
            ads = rewardedVideo;
            return ads.loadAsync();
        }).then(function() {
            return ads.showAsync();
        }).then(function() {
			console.log('Reward video preloaded');
            callback();
        }).catch(function(e) {
			console.log('Reward video fail ' + e.message);
        });
    }
	
function showAdsInterstitial(callback){
		var ads;
		FBInstant.getInterstitialAdAsync(idapp).then(function(interstitial){
			ads = interstitial;
			return ads.loadAsync();
		}).then(function(){
			return ads.showAsync();
		}).then(function(){
			console.log('Interstitial preloaded');
			callback();
		}).catch(function(e){
			console.log('Reward ads fail' + e.message);
		});
	}